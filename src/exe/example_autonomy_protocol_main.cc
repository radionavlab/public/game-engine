#include "../autonomy_protocol/aerial_robotics/example_autonomy_protocol.h"

const std::string autonomy_protocol_str = "example_autonomy_protocol";
using DerivedAutonomyProtocol = game_engine::ExampleAutonomyProtocol;

#include "common_autonomy_protocol_main.cc"
