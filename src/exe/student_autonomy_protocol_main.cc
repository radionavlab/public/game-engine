#include "../autonomy_protocol/aerial_robotics/student_autonomy_protocol.h"

const std::string autonomy_protocol_str = "student_autonomy_protocol";
using DerivedAutonomyProtocol = game_engine::StudentAutonomyProtocol;

#include "common_autonomy_protocol_main.cc"
